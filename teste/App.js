import React, { Component } from "react";

import "./App.css";

class App extends Component {
  state = {
    posts: [],
  };

  componentDidMount() {
    this.loadPosts();
  }

  loadPosts = async () => {
    const postsRes = fetch("https://jsonplaceholder.typicode.com/posts");
    const photosRes = fetch("https://jsonplaceholder.typicode.com/photos");

    const [posts, photos] = await Promise.all([postsRes, photosRes]);

    const postJson = await posts.json();
    const photosJson = await photos.json();

     const postAndPhotos = postJson.map((posts,index) =>{
      return{ ...posts, cover:photosJson[index].url }
     });


    this.setState({ posts: postAndPhotos });
  };

  render() {
    const { posts } = this.state;

    return (
      <section className='container'>
       <div className="posts">
        {posts.map((post) => (

        <div className='post' key={post.id}>
          <img src={post.cover} alt="omd"/>

          <div  className="post__content">
            <h2>{post.id}</h2>
            <h2>{post.title}</h2>
            <p>{post.body}</p>
          </div>
        </div>
          
        ))}
      </div>
      </section>
     
    );
  }
}

export default App;

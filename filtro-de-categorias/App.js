import { Post } from "../../components/posts";
import { loadPosts } from "../../Utils/Apis";
import "../../styles/style.css";
import { Button } from "../../components/Button";
import {  TextInput } from "../../components/TextInput";
import { useState } from 'react';
import { useEffect, useCallback } from 'react';


export const App = () =>{

  const [posts, setPosts] = useState([]);
  const [allPosts, setAllPosts] = useState([]);
  const [page, setPage] = useState(0);
  const [postsPerPage] = useState(4);
  const [searchValue, setSearchValue] = useState('');

  const noMorePost = page + postsPerPage >= allPosts.length;



  const filteredPost = !!searchValue ? 
  allPosts.filter(post => {
    return post.title.toLowerCase().includes(searchValue.toLowerCase())

  })
  :posts;


  //async componentDidMount() {
   // await this.loadPosts();
  //}





 const handleLoadPosts = useCallback( async (page, postsPerPage) => {
    const postsAndPhotos = await loadPosts();
   

    setPosts(postsAndPhotos.slice(page, postsPerPage))
    setAllPosts(postsAndPhotos)
  },[]);

  useEffect(() => {
    handleLoadPosts(0, postsPerPage);
  },[handleLoadPosts,postsPerPage]);

 const loadMorePosts = () => {

    const nextPage = page + postsPerPage;
    const nextPosts = allPosts.slice(nextPage, nextPage + postsPerPage);

    posts.push(...nextPosts);


    setPosts(posts)
    setPage(nextPage)
    
  };

 const handleChange = (e) => {
  setSearchValue(e.target.value)
  }



  return (
       
    <section className="container">


      <TextInput search={searchValue}
       handleClick={(e)=>handleChange(e)}  />


        {!!searchValue && (
          <>
            <h1> Resultado por: {searchValue}</h1>
          </>
        )}
    
      {filteredPost.length > 0 ?(
         <Post  filteredPost={filteredPost} />
         
      ):(
          <h2 style={{textAlign:"center", fontSize:"3rem"}}>nada encontrado</h2>
      )}

        


        

      {!searchValue &&(
           <div className="button__contaner">
           <Button disabled={noMorePost} onClick={loadMorePosts} text={"Ver Mais"} />
         </div>
      )}

    

    </section>
  );

}



/* class App2 extends Component {
  state = {
    posts: [],
    allPosts: [],
    page: 0,
    postsPerPage: 4,
    searchValue : "",
  };

  async componentDidMount() {
    await this.loadPosts();
  }

  loadPosts = async () => {
    const { page, postsPerPage } = this.state;
    const postsAndPhotos = await loadPosts();
    this.setState({
      posts: postsAndPhotos.slice(page, postsPerPage),
      allPosts: postsAndPhotos,
    });
  };

  loadMorePosts = () => {
    const { page, postsPerPage, allPosts, posts } = this.state;

    const nextPage = page + postsPerPage;
    const nextPosts = allPosts.slice(nextPage, nextPage + postsPerPage);

    posts.push(...nextPosts);

    this.setState({ posts, page: nextPage });
    
  };



  handleChange = (e) => {
    this.setState({ searchValue: e.target.value });
  }


  
  render() {
    const { posts, page, postsPerPage, allPosts,searchValue} = this.state;
    const noMorePost = page + postsPerPage >= allPosts.length;


    const filteredPost = !!searchValue ? 
    allPosts.filter(post => {
      return post.title.toLowerCase().includes(searchValue.toLowerCase())

    })
    :posts;




    return (
       
      <section className="container">


        <TextInput search={searchValue}
         handleClick={(e)=>this.handleChange(e)}  />

         




          {!!searchValue && (
            <>
              <h1> Resultado por: {searchValue}</h1>
            </>
          )}
      
        {filteredPost.length > 0 ?(
           <Post  filteredPost={filteredPost} />
           
        ):(
            <h2 style={{textAlign:"center", fontSize:"3rem"}}>nada encontrado</h2>
        )}

          


          

        {!searchValue &&(
             <div className="button__contaner">
             <Button disabled={noMorePost} onClick={this.loadMorePosts} text={"Ver Mais"} />
           </div>
        )}

      

      </section>
    );
  }
} */

export default App;

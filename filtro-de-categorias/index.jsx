import "./style.css";
export  const Post = ({ filteredPost }) => {

    return(
      <div className="posts">
        {filteredPost.map(post => (
       <div key={post.id}  className="post__content">
       <img src={post.cover} alt="foto" />
       <h1>{post.title}</h1>
       <p>{post.body}</p>
       
       </div>
        ))}

      </div>
      
     
    )
  }

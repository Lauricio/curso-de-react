
import React,{useState} from "react";
import "./index.css"

export default function App(){

  const carros = [
    {categoria:"Esporte",preco:"20334",modelo:"ford Ka"},
    {categoria:"Esporte",preco:"10234",modelo:"palio wekeend"},
    {categoria:"Ultilitario",preco:"331124",modelo:"hillux"},
    {categoria:"Ultilitario",preco:"102334",modelo:"fiat touro"},
    {categoria:"Suv",preco:"24334",modelo:"t-cross"},
    {categoria:"Suv",preco:"235334",modelo:"uno"}
  ]

  
  const linhas = (cat) =>{
    const li = []
    carros.forEach(
    (carro)=>{
      if(carro.categoria.toUpperCase() == cat.toUpperCase() || cat == ""){
        li.push(
          <tr>
            <td>{carro.categoria}</td>
            <td>{carro.preco}</td>
            <td>{carro.modelo}</td>
          </tr>
        )
      }
    }
    )
    
    return li
  }


  const tabelaCarros = (cat) => {

  return(
    <table border="1" >
      <thead>
      <tr>
        <th>categoria</th>
        <th>preco</th>
        <th>modelo</th>
      </tr>
      </thead>
      <tbody>
            {linhas(cat)}
      </tbody>
    </table>
  )

  }

 const searchCategory = (cat, setCat) =>{
  return (
    <div>
       <input type="text"  value={cat}  onChange={(e)=>setCat(e.target.value)} />
    </div>
  )
 }

 

  const [categoria,setCategoria] = useState("");

  return(
    <div className="container">
         {searchCategory(categoria,setCategoria)}
         {tabelaCarros(categoria)}
    </div>
  )
}

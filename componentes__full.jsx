import { Component } from "react"; 

import "./App.css"

import { loadPosts } from "../../FunctionsOut/loadPosts"
import  Post from "../Posts/index"
import { Button } from "../Button";

class App extends Component {

  state = {
    posts:[],
    allPosts:[],
    page:0,
    postsPerPage:50
  }


 async componentDidMount(){
  await  this.loadPosts();
  }


  loadPosts = async () =>{
  const { page,postsPerPage } = this.state;


   const postsPhotos = await loadPosts();
   this.setState({
     posts:postsPhotos.slice(page,postsPerPage),
     allPosts:postsPhotos
    })

   
  }

loadMorePosts = () =>{
  const {
    page,
    postsPerPage,
    allPosts,
    posts
  } = this.state

  const nextPage = page + postsPerPage;
  const nextPosts = allPosts.slice(nextPage,nextPage + postsPerPage)
  posts.push(...nextPosts);

  this.setState(
    { posts,page: nextPage }
  )
}



  render(){
      const { posts , page, postsPerPage, allPosts } = this.state;
      const noMorePosts = page + postsPerPage >= allPosts.length;
    return(
      <section className="container">
       <Post post={posts}/>
       <Button disabled={noMorePosts} onClick={this.loadMorePosts} text="ler mais"/>
      </section>
      
    )
  }
}

export default App;


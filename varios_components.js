
import { Component } from "react"; 

import "./App.css"

import { loadPosts } from "./FunctionsOut/loadPosts"
import  Post from "./components/Posts/index"

class App extends Component {

  state = {
    posts:[]
  }


 async componentDidMount(){
  await  this.loadPosts();
  }


  loadPosts = async () =>{
   const postsPhotos = await loadPosts();
   this.setState({ posts:postsPhotos})

   
  }
  render(){
      const { posts } = this.state
    return(
      <section className="container">
       <Post post={posts}/>
      </section>
      
    )
  }
}

export default App;


======================================================================================




export const PostCard = ({title,body,cover}) => {
    return(
       <div  className="post">
       <img src={cover} alt={title}/>
        <div  className="posts_content">
        <h1 > {title}</h1>
        <p>{body}</p>

       </div>
     </div>

    )
}



====================================================================================




import  { PostCard } from "../PostCard"

export default function Post({ post }){
    return(
        <div className="posts">
        {post.map(posts => (
        <PostCard 
        key={posts.id}
        title={posts.title}
        body={posts.body}
        id={posts.id}
        cover={posts.cover}
        />
        
        ))}
        </div>
    )

  
}



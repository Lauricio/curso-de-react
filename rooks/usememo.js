import { useEffect, useMemo, useState } from "react";
import P from "prop-types";



const Post = ( { post }) =>{

  console.log("filho renderizou ")


  return(
    <div key={post.id}>
    <h1>{post.title}</h1>
    <p>{post.body}</p> 
  </div>
  )

}

Post.prototypes = {
post:P.shape({
  id:P.number,
  title:P.string,
  body:P.string,
})
}


function App() {

const [posts, setPosts] = useState([]);
const [values, setValues] = useState("");

  console.log("pai renderizou ");


//component did mount

useEffect(() => {
  setTimeout(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
    .then((res) => res.json())
    .then((res) => setPosts(res)); 
  }, 3000);
}, []);

  return (
    <div>
      <p>
        <input type="search" onChange={(e) => setValues(e.target.value)}/>
      </p>

      
      {useMemo(() => {
          return(
          posts.length > 0 && posts.map(post => 
           <Post post={post} key={post.id}  />
          )  
      )
      },[posts])}

      {posts.length <= 0 && (
        <p>carregando posts</p>
      ) }
    </div>
  );
}

export default App;


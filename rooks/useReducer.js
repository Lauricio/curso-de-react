
const { useReducer } = require('react')


const global = {
  title:"lauricio mestre",
  body:"corpo do mestre",
} 


const reducer = (state, action) =>{
  switch (action.type) {
    case "mestre mudou":
      console.log("chamou mestre");
      return {...state, title:"lauricio é o rei"};
      
      break;
  
    default:
      break;
  }
  return {...state}
}

function App() {
  //como primeiro parametro passa a função
  const [state, dispatch] = useReducer(reducer,global);

  return(
    <>
    <h3>{state.title}</h3>
    <button onClick={() => dispatch({type: "mestre mudou"})}>click</button>

    </>
  )
  
}

export default App;

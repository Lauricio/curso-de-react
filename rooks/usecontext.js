import React, { useContext }  from "react";

const globalState = {
  title:"este é o titulo",
  state:0,
}

const globalKey = {
  title:"chave title",
}

const GlobalContext = React.createContext();


const Div = () =>{
  return   <H1/>
}

const H1 = () =>{
  const theContextValue = useContext(GlobalContext)
  return <h1> {theContextValue.title} </h1>
}




const App = () =>{
  return(
    <GlobalContext.Provider value={globalState, globalKey}>
     <Div/>
    </GlobalContext.Provider>
  )
}

export default App;

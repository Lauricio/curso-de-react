import P from 'prop-types';
import React, { useCallback, useState } from 'react';



const Button = React.memo( function Button ({ increment }){
  return <button onClick={() => increment(5)}>+</button>
  console.log("filho renderizou")

});

Button.propTypes = {
  increment:P.func,
}

function App() {
  const [counter, setCounter] = useState(0);

  const incrementCounter = useCallback((num) =>{
    setCounter((c) =>  c + num);
  },[]);
 
  console.log("pai renderizou");

  return (
   <div>
   <p>teste 3</p>
   <h1>c1: {counter}</h1>
    <Button increment={incrementCounter}   />
   </div>
  );
}

export default App;

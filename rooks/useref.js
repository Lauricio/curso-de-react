
import { useEffect, useMemo, useRef, useState } from "react";

const App = function () {
  const [posts, setPosts] = useState([]);
  const [value, setValue] = useState("");
  const inputReference = useRef(null)

  useEffect(() => {
    console.log("filho renderizou")
        fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((res) => setPosts(res));
  }, []);

  useEffect(() => {
   // inputReference.current.value = value
    inputReference.current.focus();

  }, [value])

  console.log("pai renderizou !");

  const handleValue = (val) =>{
   setValue(val)
   
   
  }

  return (
    <>
      <p>
        <input
         ref={inputReference}
         type="search"
         onChange={(e) => setValue(e.target.value)} />
      </p>

      {useMemo(
        () =>
          posts.map((post) => (
            <div key={post.id}>
              <h1 onClick={() => handleValue(post.title)}>{post.title}</h1>
              <p>{post.body}</p>
            </div>
          )),[posts]
      )}

      {}
    </>
  );
};

export default App;
